#!/bin/bash
#set -x

APP=./cmake-build-release/contrast_enhance




GEOMETRY="1100x900+5270+2520" 

for MODALITY in Aqua Cy5 DAPI FITC Gold Oran
do

echo $MODALITY 

INDIR=/home/bapy/storage/la/8bit/${MODALITY}_${GEOMETRY}
OUTDIR=/home/bapy/data/la/sda/8bit/${MODALITY}_${GEOMETRY}/rescaled

mkdir -p "$OUTDIR"

$APP -I "$INDIR" \
-O "$OUTDIR"

done
