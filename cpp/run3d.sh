#!/bin/bash
#set -x

APP=./cmake-build-release/sda3d
GEOMETRY="1100x900+5270+2520"

MODALITY=$1
#for MODALITY in Aqua Cy5 DAPI FITC Gold Oran
#do
for RADIUS in 1 10 20 40
do
for THRESH in 0 1 5 10 #20 40 60 80 
do

echo $MODALITY $RADIUS $THRESH
#MODALITY=DAPI
#THRESH=20
#RADIUS=20

INDIR=/home/bapy/storage/la/8bit/${MODALITY}_${GEOMETRY}
RESCALEDIR=/home/bapy/data/la/sda/results_$GEOMETRY/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}/r
OUTDIR=/home/bapy/data/la/sda/results_$GEOMETRY/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}/o
Z_SCALE=6

mkdir -p "$OUTDIR" 
mkdir -p "$RESCALEDIR"

Z=`echo $RADIUS/$Z_SCALE | bc -l`

$APP -I "$INDIR" \
-O "$OUTDIR" \
-R "$RESCALEDIR" \
-r $RADIUS -z $Z \
-t $THRESH  2>>errors3d_$MODALITY.log

done
done
#done
