#!/bin/bash
#set -x

APP=./cmake-build-release/sda2d
GEOMETRY="1100x900+5270+2520" 

for MODALITY in Aqua Cy5 DAPI FITC Gold Oran
do
for THRESH in 0 1 5 10 20 40 60 80
do
for RADIUS in 1 10 20 40 60 80
do

echo $MODALITY $RADIUS $THRESH

INDIR=/home/bapy/storage/la/8bit/${MODALITY}_${GEOMETRY}
OUTDIR=/home/bapy/data/la/sda/results_$GEOMETRY/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}
FILENAME="Slide 11 from cassette 1-Region 001-R1 1_${MODALITY}.tif"

mkdir -p "$OUTDIR"

$APP -i "$INDIR/$FILENAME" \
-o "$OUTDIR/o_$FILENAME" \
-s "$OUTDIR/r_$FILENAME" \
-r $RADIUS \
-t $THRESH  2>>errors_$MODALITY.log

done
done
done
