REGION=500x500-0-0
REGION=1100x900+5270+2520
BASEDIR=/home/bapy/data/la/sda
OUTPUT_DIR=$BASEDIR/results_$REGION
mkdir -p "$OUTPUT_DIR"

for MODALITY in Aqua Cy5 DAPI FITC Gold Oran
do
    echo montage -tile 9x4 -geometry 200x200+3+2 \\
    ROOT=$BASEDIR/results_$REGION/${MODALITY}_$REGION
    cd $ROOT
    #set -x
    for RADIUS in 1 10 20 40
    do
        echo -e  "-label original  \"$ROOT/rescaled/Slide 11 from cassette 1-Region 001-R1 1_$MODALITY.tif\" "   "\\" 
        for THRESHOLD in 0 1 5 10 20 40 60 80
        do
            INDIR=r${RADIUS}t${THRESHOLD}
            #ls "$ROOT/$INDIR/r/Slide 11 from cassette 1-Region 001-R1 1_$MODALITY.tif" > /dev/null
            echo -e  "-label $INDIR \"$ROOT/$INDIR/r/Slide 11 from cassette 1-Region 001-R1 1_$MODALITY.tif\" "   "\\" 
        done
    done
    echo $OUTPUT_DIR/${MODALITY}_3d.png
    echo ""
done


# sposób wywołania: 
# bash montage.sh > montage.run
# . montage.run

#jeżeli gdzieś ma być dziura, to trzeba jawnie w tym miejscu podać null:

# montage -tile 4x5 obrazek1.jpg ... obrazek2.jpg ... null: null: obrazekX.jpg ...
