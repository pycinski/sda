# wycinanie fragmentów obrazów

SIZE=1100x900
ORIGIN=+5270+2520
REGION=${SIZE}${ORIGIN}

cd 8bit

for DIR in Aqua   Cy5   DAPI   FITC   Gold   Oran  ; do
#DIR=Aqua
  mkdir -p ${DIR}_${REGION}
  ls $DIR | while read f; do  convert "$DIR/$f" -crop $REGION  "${DIR}_${REGION}/$f"; done
done
