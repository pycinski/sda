//
// Created by bapy on 05.03.2020.
//

#include "itkConstShapedNeighborhoodIterator.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"
#include <itkImageSeriesReader.h>
#include <itkImageSeriesWriter.h>
#include <itkNumericSeriesFileNames.h>
#include <itkRegularExpressionSeriesFileNames.h>
#include <itkRescaleIntensityImageFilter.h>

#include "options.hpp"
#include <libgen.h>

#include <itkTIFFImageIO.h>

/*
#include <itkMetaImageIO.h>
#include <itkPNGImageIO.h>

static bool endsWith(const std::string& str, const std::string& suffix)
{
  return str.size() >= suffix.size() && 0 ==
str.compare(str.size()-suffix.size(), suffix.size(), suffix);
}

static bool endsWithCaseInsensitive(const std::string&str, const
std::string&suffix) { return str.size() >= suffix.size() && std::equal(
      suffix.rbegin(), suffix.rend(), str.rbegin(),
                    [](const char a, const char b) {
                      return tolower(a) == tolower(b);
                    }
  );
}
*/

int main(int argc, char **argv) {
  sweet::Options opt(argc, const_cast<char **>(argv), "Description:");

  std::string input_dirname;
  std::string output_directory;

  opt.get("-I", "--input-dir", "Input directory", input_dirname);
  opt.get("-O", "--output-directory", "Output directory", output_directory);

  opt.finalize();

  if (input_dirname.empty() || output_directory.empty()) {
    std::cerr << "Argument error! Run: \n"
              << argv[0] << " --help \nto see valid options.\n";
    return 1;
  }

  constexpr unsigned int Dimension = 3;
  using InputPixelType = unsigned short;
  using OutputPixelType = unsigned short;
  using InputImageType = itk::Image<InputPixelType, Dimension>;
  using OutputImageType = itk::Image<OutputPixelType, Dimension>;
  using OutputImage2DType = itk::Image<OutputPixelType, Dimension - 1>;

  using ShapedNeighborhoodIteratorType =
      itk::ConstShapedNeighborhoodIterator<InputImageType>;

  using OutputIteratorType = itk::ImageRegionIterator<OutputImageType>;

  using NameGeneratorType = itk::RegularExpressionSeriesFileNames;
  NameGeneratorType::Pointer nameGenerator = NameGeneratorType::New();

  nameGenerator->SetRegularExpression(".*");
  nameGenerator->SetDirectory(input_dirname);
  nameGenerator->SetNumericSort(false);
  nameGenerator->SetSubMatch(0); // sort by whole filenames

  auto input_filenames = nameGenerator->GetFileNames();

  using ReaderType = itk::ImageSeriesReader<InputImageType>;

  ReaderType::Pointer reader = ReaderType::New();
  /* if (endsWithCaseInsensitive(input_filenames[0], ".tif")
   || endsWithCaseInsensitive(input_filenames[0], ".tiff")
   ) {
     reader->SetImageIO(itk::TIFFImageIO::New());
   }
   else if (endsWithCaseInsensitive(input_filenames[0], ".png")
   ) {
     reader->SetImageIO(itk::PNGImageIO::New());
   }
   else if (endsWithCaseInsensitive(input_filenames[0], ".mha")
         || endsWithCaseInsensitive(input_filenames[0], ".mhd")
         ) {
       reader->SetImageIO(itk::MetaImageIO::New());
   }
 */
  reader->SetFileNames(input_filenames);

  try {
    reader->Update();
  } catch (const itk::ExceptionObject &err) {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }

/*
  OutputImageType::Pointer output = OutputImageType::New();
  output->SetRegions(reader->GetOutput()->GetRequestedRegion());
  output->Allocate();
*/

  using SeriesWriterType =
      itk::ImageSeriesWriter<OutputImageType, OutputImage2DType>;
  using RescaleFilterType =
      itk::RescaleIntensityImageFilter<OutputImageType, OutputImageType>;
  RescaleFilterType::Pointer rescaler = RescaleFilterType::New();

  /*
    SeriesWriterType::Pointer outputSeriesWriter = SeriesWriterType::New();
    outputSeriesWriter->SetInput(output);

    auto writer_image_io = itk::TIFFImageIO::New();
    writer_image_io->SetCompressionToDeflate();
    outputSeriesWriter->SetUseCompression(true);
    outputSeriesWriter->SetImageIO(writer_image_io);

    std::vector<std::string> output_filenames;
    for (const auto& in_filename : input_filenames) {
      std::string file_name = basename(const_cast<char*>(in_filename.c_str()));
      output_filenames.push_back(output_directory+"/"+ file_name); //
    NOLINT(performance-inefficient-string-concatenation)
    }
    outputSeriesWriter->SetFileNames(output_filenames);
  */
  SeriesWriterType::Pointer outputRescaledSeriesWriter =
      SeriesWriterType::New();

  std::vector<std::string> output_rescaled_filenames;
  rescaler->SetOutputMinimum(0);
  rescaler->SetOutputMaximum(itk::NumericTraits<OutputPixelType>::max() - 1);
  rescaler->SetInput(reader->GetOutput());

  for (const auto &in_filename : input_filenames) {
    std::string file_name = basename(const_cast<char *>(in_filename.c_str()));
    output_rescaled_filenames.push_back(output_directory + "/" +file_name); // NOLINT(performance-inefficient-string-concatenation)
  }

  outputRescaledSeriesWriter->SetFileNames(output_rescaled_filenames);
  outputRescaledSeriesWriter->SetInput(rescaler->GetOutput());
  auto rescaled_writer_io = itk::TIFFImageIO::New();
  rescaled_writer_io->SetCompressionToDeflate();
  outputRescaledSeriesWriter->SetUseCompression(true);
  outputRescaledSeriesWriter->SetImageIO(rescaled_writer_io);

  try {
    outputRescaledSeriesWriter->Update();
  } catch (const itk::ExceptionObject &err) {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
