
#include "itkConstShapedNeighborhoodIterator.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"
#include <itkImageSeriesReader.h>
#include <itkImageSeriesWriter.h>
#include <itkNumericSeriesFileNames.h>
#include <itkRegularExpressionSeriesFileNames.h>
#include <itkRescaleIntensityImageFilter.h>

#include "options.hpp"
#include <libgen.h>

#include <itkTIFFImageIO.h>


/*
#include <itkMetaImageIO.h>
#include <itkPNGImageIO.h>

static bool endsWith(const std::string& str, const std::string& suffix)
{
  return str.size() >= suffix.size() && 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
}

static bool endsWithCaseInsensitive(const std::string&str, const std::string&suffix) {
  return str.size() >= suffix.size() && std::equal(
      suffix.rbegin(), suffix.rend(), str.rbegin(),
                    [](const char a, const char b) {
                      return tolower(a) == tolower(b);
                    }
  );
}
*/

int
main(int argc, char ** argv)
{
  sweet::Options opt(argc, const_cast<char**>(argv), "Description:");

  std::string input_dirname;
  std::string output_directory;
  std::string rescaled_directory;
  unsigned limit_slices = 0;
  double element_radius = 0;
  double element_radius_x = 0;
  double element_radius_y = 0;
  double element_radius_z = 0;
  int threshold = 0;
  std::string regular_expression = ".*";
  double spacing_x = 1.0;
  double spacing_y = 1.0;
  double spacing_z = 1.0;

  opt.get("-I", "--input-dir", "Input directory", input_dirname);
  opt.get("-O", "--output-directory", "Output directory", output_directory);
  opt.get("-r", "--radius", "Radius in [mm]", element_radius);

  opt.get("", "--radius-x", "X radius in [mm] (optional, overrides --radius)", element_radius_x);
  opt.get("", "--radius-y", "Y radius in [mm] (optional, overrides --radius)", element_radius_y);
  opt.get("", "--radius-z", "Z radius in [mm] (optional, overrides --radius)", element_radius_z);
  opt.get("-t", "--threshold", "Intensity threshold (optional)", threshold);
  opt.get("-R", "--rescaled-directory", "Output directory for rescaled to full intensity (optional)", rescaled_directory);
  opt.get("-l", "--limit", "Limit input image to given number of slices (optional)", limit_slices);
  opt.get("-e", "--regex", "Regular expression (optional, defaults to '.*')", regular_expression);
  opt.get("-x", "", "X spacing [mm/pix] (optional, defaults to 1.0)", spacing_x);
  opt.get("-y", "", "Y spacing [mm/pix] (optional, defaults to 1.0)", spacing_y);
  opt.get("-z", "", "Z spacing [mm/pix] (optional, defaults to 1.0)", spacing_z);


  opt.finalize();

  if (input_dirname.empty() || output_directory.empty() || element_radius==0.0) {
    std::cerr << "Argument error! Run: \n" << argv[0] << " --help \nto see valid options.\n";
    return 1;
  }

  constexpr unsigned int Dimension = 3;
  using InputPixelType = unsigned short;
  using OutputPixelType = unsigned short;
  using InputImageType = itk::Image<InputPixelType, Dimension>;
  using OutputImageType = itk::Image<OutputPixelType, Dimension>;
  using OutputImage2DType = itk::Image<OutputPixelType, Dimension-1>;

  using ShapedNeighborhoodIteratorType =
    itk::ConstShapedNeighborhoodIterator<InputImageType>;

  using OutputIteratorType = itk::ImageRegionIterator<OutputImageType>;

  using NameGeneratorType = itk::RegularExpressionSeriesFileNames;
  NameGeneratorType::Pointer nameGenerator = NameGeneratorType::New();

  nameGenerator->SetRegularExpression(regular_expression);
  nameGenerator->SetDirectory(input_dirname);
  nameGenerator->SetNumericSort(false);
  nameGenerator->SetSubMatch(0);  //sort by whole filenames

  auto input_filenames = nameGenerator->GetFileNames();
  if (limit_slices>0) {
    if (limit_slices>input_filenames.size()) {
      std::cerr << "Argument error! Slice limit exceeds number of files!\n";
      return 1;
    }
    input_filenames = std::vector<std::string>(input_filenames.begin(),
                                  input_filenames.begin() + limit_slices);
  }

  using ReaderType = itk::ImageSeriesReader<InputImageType>;

  ReaderType::Pointer reader = ReaderType::New();
 /* if (endsWithCaseInsensitive(input_filenames[0], ".tif")
  || endsWithCaseInsensitive(input_filenames[0], ".tiff")
  ) {
    reader->SetImageIO(itk::TIFFImageIO::New());
  }
  else if (endsWithCaseInsensitive(input_filenames[0], ".png")
  ) {
    reader->SetImageIO(itk::PNGImageIO::New());
  }
  else if (endsWithCaseInsensitive(input_filenames[0], ".mha")
        || endsWithCaseInsensitive(input_filenames[0], ".mhd")
        ) {
      reader->SetImageIO(itk::MetaImageIO::New());
  }
*/
  reader->SetFileNames(input_filenames);

  try
  {
    reader->Update();
  }
  catch (const itk::ExceptionObject & err)
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }

  auto input_own_spacing = reader->GetOutput()->GetSpacing();
  if  (((input_own_spacing[0] != 1.0 || spacing_x != 1.0) && input_own_spacing[0] != spacing_x)
    || ((input_own_spacing[1] != 1.0 || spacing_y != 1.0) && input_own_spacing[1] != spacing_y)
    || ((input_own_spacing[2] != 1.0 || spacing_z != 1.0) && input_own_spacing[2] != spacing_z)) {
    std::cerr << "Spacing of input image does not match command line argument!\n";
    std::cerr << "Ignoring internal spacing values: " << input_own_spacing << '\n';
  }

  OutputImageType::Pointer output = OutputImageType::New();
  output->SetRegions(reader->GetOutput()->GetRequestedRegion());
  output->Allocate();


  using FaceCalculatorType =
    itk::NeighborhoodAlgorithm::ImageBoundaryFacesCalculator<InputImageType>;

  FaceCalculatorType                         faceCalculator;
  FaceCalculatorType::FaceListType           faceList;
      FaceCalculatorType::FaceListType::iterator fit;


  OutputIteratorType out;

  double radius [Dimension];
  radius[0] = element_radius_x>0.0 ? element_radius_x : element_radius;
  radius[1] = element_radius_y>0.0 ? element_radius_y : element_radius;
  radius[2] = element_radius_z>0.0 ? element_radius_z : element_radius;

  radius[0] /= spacing_x;
  radius[1] /= spacing_y;
  radius[2] /= spacing_z;

  ShapedNeighborhoodIteratorType::RadiusType radius_integer;
  radius_integer [0] = radius[0];
  radius_integer [1] = radius[1];
  radius_integer [2] = radius[2];

  faceList = faceCalculator(reader->GetOutput(), output->GetRequestedRegion(), radius_integer);
  for (fit = faceList.begin(); fit != faceList.end(); ++fit)
  {
    ShapedNeighborhoodIteratorType it(radius_integer, reader->GetOutput(), *fit);
    out = OutputIteratorType(output, *fit);

    ShapedNeighborhoodIteratorType::OffsetType off;

    // Creates a circular structuring element by activating all the pixels less
    // than radius distance from the center of the neighborhood.
    for (int z = 0; z <= radius[2]; ++z) {
      double z_square = static_cast<double>(z*z)/(radius[2]*radius[2]);
      for (int y = 0; y <= radius[1]; ++y) {
        double y_square = static_cast<double>(y*y)/(radius[1]*radius[1]);
        for (int x = 0; x <= radius[0]; ++x) {
          double x_square = static_cast<double>(x*x)/(radius[0]*radius[0]);
          double dis = x_square + y_square + z_square;
          if (dis <= 1.0) {
            off = {{ x, y, z}};
            it.ActivateOffset(off);
            off = {{ x, y, -z}};
            it.ActivateOffset(off);
            off = {{ x, -y, z}};
            it.ActivateOffset(off);
            off = {{ x, -y, -z}};
            it.ActivateOffset(off);
            off = {{ -x, y, z}};
            it.ActivateOffset(off);
            off = {{ -x, y, -z}};
            it.ActivateOffset(off);
            off = {{ -x, -y, z}};
            it.ActivateOffset(off);
            off = {{ -x, -y, -z}};
            it.ActivateOffset(off);
          }
        }
      }
    }

    for (it.GoToBegin(), out.GoToBegin(); !it.IsAtEnd(); ++it, ++out)
    {
      ShapedNeighborhoodIteratorType::ConstIterator ci;
      int current_val = static_cast<int>(it.GetCenterPixel());
      OutputPixelType out_val = itk::NumericTraits<OutputPixelType >::Zero;
      for (ci = it.Begin(); ci != it.End(); ci++)
      {
        if (static_cast<int>(ci.Get()) <= current_val - threshold)
        {
            ++out_val;
        }
      }
      out.Set(out_val);
    }
  }

  using RescaleFilterType = itk::RescaleIntensityImageFilter<OutputImageType, OutputImageType>;
  RescaleFilterType::Pointer rescaler = RescaleFilterType::New();

  using SeriesWriterType = itk::ImageSeriesWriter<OutputImageType, OutputImage2DType>;
  SeriesWriterType::Pointer outputSeriesWriter = SeriesWriterType::New();
  outputSeriesWriter->SetInput(output);

  auto writer_image_io = itk::TIFFImageIO::New();
  writer_image_io->SetCompressionToDeflate();
  outputSeriesWriter->SetUseCompression(true);
  outputSeriesWriter->SetImageIO(writer_image_io);

  std::vector<std::string> output_filenames;
  for (const auto& in_filename : input_filenames) {
    std::string file_name = basename(const_cast<char*>(in_filename.c_str()));
    output_filenames.push_back(output_directory+"/"+ file_name); // NOLINT(performance-inefficient-string-concatenation)
  }
  outputSeriesWriter->SetFileNames(output_filenames);

  SeriesWriterType::Pointer outputRescaledSeriesWriter = SeriesWriterType::New();

  std::vector<std::string> output_rescaled_filenames;
  if (!rescaled_directory.empty()) {
    rescaler->SetOutputMinimum(0);
    rescaler->SetOutputMaximum(itk::NumericTraits<OutputPixelType>::max() - 1);
    rescaler->SetInput(output);

    for (const auto& in_filename : input_filenames) {
      std::string file_name = basename(const_cast<char*>(in_filename.c_str()));
      output_rescaled_filenames.push_back(rescaled_directory+"/"+ file_name); // NOLINT(performance-inefficient-string-concatenation)
    }
    outputRescaledSeriesWriter->SetFileNames(output_rescaled_filenames);
    outputRescaledSeriesWriter->SetInput(rescaler->GetOutput());
    auto rescaled_writer_io = itk::TIFFImageIO::New();
    rescaled_writer_io->SetCompressionToDeflate();
    outputRescaledSeriesWriter->SetUseCompression(true);
    outputRescaledSeriesWriter->SetImageIO(rescaled_writer_io);
  }

  try
  {
    outputSeriesWriter->Update();
    if (!rescaled_directory.empty()) {
      outputRescaledSeriesWriter->Update();
    }
  }
  catch (const itk::ExceptionObject & err)
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
