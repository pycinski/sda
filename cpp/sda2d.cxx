
#include "itkConstShapedNeighborhoodIterator.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"
#include "options.hpp"
#include <itkRescaleIntensityImageFilter.h>
#include <itkTIFFImageIO.h>
int
main(int argc, char ** argv)
{
  sweet::Options opt(argc, const_cast<char**>(argv), "Description:");

  std::string input_filename;
  std::string output_filename;
  std::string rescaled_filename;
  unsigned element_radius = 0;
  int threshold = 0;

  opt.get("-i", "--input", "Input image", input_filename);
  opt.get("-o", "--output", "Output image", output_filename);
  opt.get("-s", "--output-rescaled", "Output image rescaled to full intensity (optional)", rescaled_filename);
  opt.get("-r", "--radius", "Radius in [pix]", element_radius);
  opt.get("-t", "--threshold", "Intensity threshold (optional)", threshold);

  opt.finalize();

  if (input_filename.empty() || output_filename.empty() || !element_radius) {
    std::cerr << "Argument error! Run: \n" << argv[0] << " --help \nto see valid options.\n";
    return 1;
  }

  constexpr unsigned int Dimension = 2;
  using InputPixelType = unsigned short;
  using OutputPixelType = unsigned short;
  using InputImageType = itk::Image<InputPixelType, Dimension>;
  using OutputImageType = itk::Image<OutputPixelType, Dimension>;

    using ShapedNeighborhoodIteratorType =
    itk::ConstShapedNeighborhoodIterator<InputImageType>;

  using OutputIteratorType = itk::ImageRegionIterator<OutputImageType>;


  using ReaderType = itk::ImageFileReader<InputImageType>;
  ReaderType::Pointer reader = ReaderType::New();
  reader->SetFileName(input_filename);

  try
  {
    reader->Update();
  }
  catch (const itk::ExceptionObject & err)
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }

  OutputImageType::Pointer output = OutputImageType::New();
  output->SetRegions(reader->GetOutput()->GetRequestedRegion());
  output->Allocate();

  ShapedNeighborhoodIteratorType::RadiusType radius;
  radius.Fill(element_radius);

  using FaceCalculatorType =
    itk::NeighborhoodAlgorithm::ImageBoundaryFacesCalculator<InputImageType>;

  FaceCalculatorType                         faceCalculator;
  FaceCalculatorType::FaceListType           faceList;
      FaceCalculatorType::FaceListType::iterator fit;

  faceList = faceCalculator(reader->GetOutput(), output->GetRequestedRegion(), radius);

  OutputIteratorType out;

  int rad = static_cast<int>(element_radius);

  for (fit = faceList.begin(); fit != faceList.end(); ++fit)
  {
    ShapedNeighborhoodIteratorType it(radius, reader->GetOutput(), *fit);
    out = OutputIteratorType(output, *fit);

    // Creates a circular structuring element by activating all the pixels less
    // than radius distance from the center of the neighborhood.

    for (int y = -rad; y <= rad; ++y) {
      for (int x = -rad; x <= rad; ++x) {
        ShapedNeighborhoodIteratorType::OffsetType off;

        int dis = (x * x + y * y);
        if (dis <= rad*rad)
        {
          off[0] = x;
          off[1] = y;
          it.ActivateOffset(off);
        }
      }
    }

    for (it.GoToBegin(), out.GoToBegin(); !it.IsAtEnd(); ++it, ++out)
    {
      ShapedNeighborhoodIteratorType::ConstIterator ci;
      int current_val = static_cast<int>(it.GetCenterPixel());
      OutputPixelType out_val = itk::NumericTraits<OutputPixelType >::Zero;
      for (ci = it.Begin(); ci != it.End(); ci++)
      {
          //if (imgin[x + i, y + j] >= imgin[x, y] + threshold)
          //   imgout[x, y]++;
        if (static_cast<int>(ci.Get()) <= current_val - threshold)
        {
            ++out_val;
        }
      }
      out.Set(out_val);

    }

  }

  using RescaleFilterType = itk::RescaleIntensityImageFilter<OutputImageType, OutputImageType>;
  RescaleFilterType::Pointer rescaler = RescaleFilterType::New();

  using WriterType = itk::ImageFileWriter<OutputImageType>;

  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName(output_filename);
  writer->SetInput(output);

  auto writer_image_io = itk::TIFFImageIO::New();
  writer_image_io->SetCompressionToDeflate();
  writer->SetUseCompression(true);
  writer->SetImageIO(writer_image_io);

  WriterType::Pointer writer_rescaled = WriterType::New();

  if (!rescaled_filename.empty()) {
    rescaler->SetOutputMinimum(0);
    rescaler->SetOutputMaximum(itk::NumericTraits<OutputPixelType>::max() - 1);
    rescaler->SetInput(output);
    writer_rescaled->SetFileName(rescaled_filename);
    writer_rescaled->SetInput(rescaler->GetOutput());
    auto rescaled_writer_image_io = itk::TIFFImageIO::New();
    rescaled_writer_image_io->SetCompressionToDeflate();
    writer_rescaled->SetUseCompression(true);
    writer_rescaled->SetImageIO(rescaled_writer_image_io);
  }
  try
  {
    writer->Update();
    if (!rescaled_filename.empty()) {
      writer_rescaled->Update();
    }
  }
  catch (const itk::ExceptionObject & err)
  {
    std::cerr << "ExceptionObject caught !" << std::endl;
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
