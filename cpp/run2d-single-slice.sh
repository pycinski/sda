#!/bin/bash
#set -x

APP=./cmake-build-release/sda2d
GEOMETRY=full

########################
MODALITY=Cy5
RADIUS=20
THRESH=60
echo $MODALITY $RADIUS $THRESH

INDIR=/home/bapy/storage/la/8bit/${MODALITY}
OUTDIR=/home/bapy/data/la/sda/results_${GEOMETRY}/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}
FILENAME="Slide 05 from cassette 1-Region 001-R1 1_${MODALITY}.tif"

mkdir -p "$OUTDIR"

$APP -i "$INDIR/$FILENAME" \
-o "$OUTDIR/o_$FILENAME" \
-s "$OUTDIR/r_$FILENAME" \
-r $RADIUS \
-t $THRESH   2>>errors_$MODALITY.log

########################
MODALITY=DAPI
RADIUS=20
THRESH=20
echo $MODALITY $RADIUS $THRESH

INDIR=/home/bapy/storage/la/8bit/${MODALITY}_${GEOMETRY}
OUTDIR=/home/bapy/data/la/sda/results_${GEOMETRY}/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}
FILENAME="Slide 05 from cassette 1-Region 001-R1 1_${MODALITY}.tif"

mkdir -p "$OUTDIR"

$APP -i "$INDIR/$FILENAME" \
-o "$OUTDIR/o_$FILENAME" \
-s "$OUTDIR/r_$FILENAME" \
-r $RADIUS \
-t $THRESH  2>>errors_$MODALITY.log
########################
MODALITY=FITC
RADIUS=20
THRESH=10
echo $MODALITY $RADIUS $THRESH

INDIR=/home/bapy/storage/la/8bit/${MODALITY}_${GEOMETRY}
OUTDIR=/home/bapy/data/la/sda/results_${GEOMETRY}/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}
FILENAME="Slide 05 from cassette 1-Region 001-R1 1_${MODALITY}.tif"

mkdir -p "$OUTDIR"

$APP -i "$INDIR/$FILENAME" \
-o "$OUTDIR/o_$FILENAME" \
-s "$OUTDIR/r_$FILENAME" \
-r $RADIUS \
-t $THRESH   2>>errors_$MODALITY.log
########################
MODALITY=Aqua
RADIUS=20
THRESH=10
echo $MODALITY $RADIUS $THRESH

INDIR=/home/bapy/storage/la/8bit/${MODALITY}_${GEOMETRY}
OUTDIR=/home/bapy/data/la/sda/results_${GEOMETRY}/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}
FILENAME="Slide 05 from cassette 1-Region 001-R1 1_${MODALITY}.tif"

mkdir -p "$OUTDIR"

$APP -i "$INDIR/$FILENAME" \
-o "$OUTDIR/o_$FILENAME" \
-s "$OUTDIR/r_$FILENAME" \
-r $RADIUS \
-t $THRESH   2>>errors_$MODALITY.log
########################
MODALITY=Oran
RADIUS=20
THRESH=20
echo $MODALITY $RADIUS $THRESH

INDIR=/home/bapy/storage/la/8bit/${MODALITY}_${GEOMETRY}
OUTDIR=/home/bapy/data/la/sda/results_${GEOMETRY}/${MODALITY}_${GEOMETRY}/r${RADIUS}t${THRESH}
FILENAME="Slide 05 from cassette 1-Region 001-R1 1_${MODALITY}.tif"

mkdir -p "$OUTDIR"

$APP -i "$INDIR/$FILENAME" \
-o "$OUTDIR/o_$FILENAME" \
-s "$OUTDIR/r_$FILENAME" \
-r $RADIUS \
-t $THRESH   2>>errors_$MODALITY.log
