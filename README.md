ITK-based implementation of the SDA algorithm [Piorkowski2016]



----
Piorkowski A.: A Statistical Dominance Algorithm for Edge Detection and
Segmentation of Medical Images. AISC vol. 471, Springer 2016, pp. 3-14.
