# Do poprawienia w programie



### BUGS
- W opcjach CMake'a włączyłem moduł SCIFIO, on stał się domyślnym silnikiem czytania. Tak naprawdę to ejest zewnętrzny jar, który rzuca brakiem pamięci. Ustawić coś w stylu `JAVA_FLAGS="-Xms256m -Xmx4g"`albo `auto writer_io = itk::TIFFImageIO::New(); writer->SetImageIO(writer_io)`
- Reader jest domyślny, więc zamianst TIFFImageIO jest właśnie tej javowy. Dla większych obrazków może kończyć się błędami. Niewykluczone też, że można mocno przyspieszyć z czytaniem
- Writer domyślnie zakłada że zapisuje na TIFF-a. Bez względu na rozszerzenie (sprawdzane w 2d)



### ENHANCEMENTS
- flaga `-v, --verbose` z wyświetlaniem postępu  -- [klasa ProgressReporter](https://itk.org/Doxygen/html/classitk_1_1ProgressReporter.html)
- wielowątkowość (podział na kilka ROI i praca na nich)

